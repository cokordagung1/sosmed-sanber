@extends('layouts.master')

@section('content')
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-1"></div>
      <!-- /.col -->
      <div class="col-md-9">
        <div class="card">
          <div class="card-header p-2">
          </div><!-- /.card-header -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Show posting</h6>
            </div>
            <div class="card-body">
              <h5 style="color: black;">Bio :</h5>
             <p>The styling for this basic card example is created by using default Bootstrap utility classes.
                By using utility classes, the style of the card component can be easily modified with no need for any custom CSS!</p> 
            </div>
            <hr>
            <div class="card-body">
              <h5 style="color: black;">Photo :</h5>
              <img src="{{ asset('/img/1.jpg')}}" style="height:100px; width:100px;" alt="">
            </div>
            <a href="{{'/profiles'}}" class="btn btn-primary">back</a>
          </div>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>


@endsection