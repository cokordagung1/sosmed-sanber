@extends('layouts.master')

@section('content')
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-1">
      </div>
      <!-- /.col -->
      <div class="col-md-9">
        <div class="card">
          <div class="card-header p-2">
          </div><!-- /.card-header -->
          <form role="form" action="/post/edit/{{ $post->id }}" method="post">
              @csrf
              @method("put")
            <div class="card-body">
              <div class="form-group">
                <label for="caption">Caption</label>
                <input type="text" value="{{$post->caption}}" class="form-control" name="caption" id="caption" placeholder="caption" required>
              </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>

        </div>
        <!-- /.nav-tabs-custom -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>

@endsection
