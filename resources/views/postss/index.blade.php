@extends('layouts.master')

@section('content')
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-1"></div>
      <!-- /.col -->
      <div class="col-md-9">
        <div class="card">
          <div class="card-header p-2">
            <ul class="nav nav-pills">
              {{-- <li class="nav-item"><a class="nav-link active" href="#activity">Activity</a></li> --}}
              <li class="nav-item"><a class="btn btn-primary" href="/post/create">Create Post </a></li>
            </ul>
          </div><!-- /.card-header -->

          @foreach($post as $p)
          <div class="card-body">
            <div class="tab-content">
              <div class="active tab-pane" id="activity">
                <!-- Post -->
                <div class="post">
                  <div class="user-block">
                    <img class="img-circle img-bordered-sm" src="{{url('/profile_img/'.$p->users->userdetail->img)}}" alt="user image">
                    <span class="username">
                      <a href="/profiles/{{$p->users->id}}">{{$p->users->name}}</a>
                    </span>

                  </div>
                  <!-- /.user-block -->
                    @if($p->image != null)
                    <img style="max-height: 250px;" src="{{'/img_post/'.$p->image}}">
                    @endif

                  <p>
                    {{$p->caption}}
                  </p>

                  <p>
                    <a href="/post/like/{{$p->id}}/{{Auth::id()}}" id="likebtn{{$p->id}}" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"> Like ({{count($p->postlikeid)}})</i></a>
                      <a href="/post/liked/{{$p->id}}/{{Auth::id()}}" style="display: none" id="likedbtn{{$p->id}}"class="link-black text-sm"><i class="far fa-thumbs-up mr-1"> Liked ({{count($p->postlikeid)}})</i></a>

                      @foreach($p->postlikeid as $h)
                          <script>
                              if({{$h->pivot->users_id == Auth::id()}}){
                                  document.getElementById("likebtn{{$p->id}}").style.display = "none";
                                  document.getElementById("likedbtn{{$p->id}}").style.display = "block";
                              }else{

                              }
                          </script>
                      @endforeach

                    <span class="float-right">
                      <a href="" class="link-black text-sm">
                        <i class="far fa-comments mr-1"> Comments({{count($p->comment)}})</i>
                      </a>


                    </span>
                  </p>
                    @foreach($p->comment as $t)
                    <p><a href="/profiles/{{$t->users->name}}">{{$t->users->name}}</a>:   {{$t->comment}}
                        <a href="/comments/like/{{$t->id}}/{{Auth::id()}}" id="likebtncomment{{$t->id}}" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"> Like ({{count($t->commentlike)}})</i></a>
                        <a href="/comments/liked/{{$t->id}}/{{Auth::id()}}" style="display: none" id="likedbtncomment{{$t->id}}" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"> Liked ({{count($t->commentlike)}})</i></a>


                    </p>
                        @foreach($t->commentlike as $a)
                        <script>
                            if({{$a->pivot->users_id == Auth::id()}}){
                                document.getElementById("likebtncomment{{$t->id}}").style.display = "none";
                                document.getElementById("likedbtncomment{{$t->id}}").style.display = "inline-block";
                            }else{

                            }
                        </script>

                        @endforeach


                        @endforeach



                  <form class="form-horizontal" method="post" action="/comments/comment/{{$p->id}}/{{Auth::id()}}">
                      @csrf
                    <div class="input-group input-group-sm mb-0">
                      <input class="form-control form-control-sm" placeholder="Response" name="comment">
                      <div class="input-group-append">
                        <button type="submit" class="btn btn-danger">Send</button>
                      </div>
                    </div>
                  </form>
                </div>
                <!-- /.post -->

                <!-- Post -->
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div><!-- /.card-body -->

                @endforeach

        </div>
        <!-- /.nav-tabs-custom -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
</section>
@endsection
