@extends('layouts.master')

@section('content')
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-1">
      </div>
      <!-- /.col -->
      <div class="col-md-9">
        <div class="card">
          <div class="card-header p-2">
          </div><!-- /.card-header -->
          <div class="col-12">      
              <div class="col-12">
                  <div class="post">
                    <div class="user-block">
                      <img class="img-circle img-bordered-sm" src="{{ asset('img/2.jpg') }}" alt="user image">
                      <span class="username">
                        <a href="#">Fahmi fiqih masriza</a>
                      </span>
                      <span class="description">7:45 PM today</span>
                    </div>
                    <!-- /.user-block -->
                    <p>
                      Lorem ipsum represents a long-held tradition for designers,
                      typographers and the like. Some people hate it and argue for
                      its demise, but others ignore.
                    </p>
    
                    <p>
                      <div class="btn-group btn-group-sm">
                        <a href="{{ url('/')}}" class="btn btn-info"><i class="fas fa-angle-left"> back</i></a>
                      </div>
                    </p>
                  </div>              
          </div>
        </div>
        <!-- /.nav-tabs-custom -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>

@endsection
