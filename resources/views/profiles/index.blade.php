@extends('layouts.master')

@section('content')

<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">

        <!-- Profile Image -->
        <div class="card card-light card-outline">
          <div class="card-body box-profile">
            <div class="text-center">
              <img class="profile-user-img img-fluid img-circle" src="{{ url('/profile_img/'.$user->userdetail->img) }}" alt="User profile picture">
            </div>

            <h3 class="profile-username text-center">{{ $user -> name }} </h3>

            <p class="text-muted text-center">Pengguna</p>

            <ul class="list-group list-group-unbordered mb-3">

              <li class="list-group-item">
                <b>Bio </b> <a class="float-right">{{$user->userdetail->bio }}</a>
              </li>
              <li class="list-group-item">
                <b>Followers</b> <a class="float-right">{{count($user->followerid)}}</a>
              </li>
            </ul>



            <a href="/profiles/edit/{{$user->id}}" id="profilebtn" class="btn btn-primary btn-block"><b>Edit Profile</b></a>

            <a href="/profiles/follow/{{Auth::id()}}/{{$user->id}}" id="follow" style="display: none" class="btn btn-primary btn-block"><b>Follow</b></a>
              <a href="/profiles/unfollow/{{Auth::id()}}/{{$user->id}}" id="unfollow" style="display: none" class="btn btn-primary btn-block"><b>UnFollow</b></a>


          </div>
          <!-- /.card-body -->
        </div>

      </div>
      <!-- /.col -->
      <div class="col-md-9">
        <div class="card">
          <div class="card-header p-2">
            <ul class="nav nav-pills">
              <li class="nav-item"><a class="btn btn-dark" href="#activity" data-toggle="tab">Activity</a></li>
            </ul>
          </div><!-- /.card-header -->
          <div class="card-body">
            <div class="tab-content">
              <div class="active tab-pane" id="activity">



                  <script>
                      if({{$user->id != Auth::id()}}){

                          document.getElementById("follow").style.display = "block";
                      }else{

                      }



                  </script>
                <!-- Post -->





                  @foreach($post as $p)
                <div class="post">
                  <div class="user-block">
                    <img class="img-circle img-bordered-sm" src="{{url('/profile_img/'.$user->userdetail->img)}}" alt="user image">
                    <span class="username">
                      <a href="#">{{$user -> name}}</a>
                    </span>
                    <span class="description">Shared publicly - 7:30 PM today</span>
                  </div>
                  <!-- /.user-block -->
                    @if($p->image != null)
                        <img style="max-width: 250px" src="{{'/img_post/'.$p->image}}">
                    @endif
                  <p>
                    {{$p->caption}}
                  </p>


                    <div class="btn-group btn-group-sm" id="test{{$p->id}}">
                      <a href="/post/edit/{{$p->id}}" class="btn btn-success"><i class="fas fa-edit"></i></a>
                      <a href="/post/delete/{{$p->id}}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                    </div>


                    <script>
                        if({{$p->users->id != Auth::id()}}){
                            document.getElementById("test{{$p->id}}").style.display = "none";
                            document.getElementById("follow").style.display = "block";
                        }else{

                        }


                    </script>



                </div>
                <!-- /.post -->
                  @endforeach

                  @foreach($user->followerid as $h)
                      <script>
                          if({{$h->pivot->users_id == Auth::id()}}){
                              document.getElementById("follow").style.display = "none";
                              document.getElementById("unfollow").style.display = "block";
                          }else{

                          }


                      </script>
              @endforeach


                <!-- Post -->
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div><!-- /.card-body -->
        </div>
        <!-- /.nav-tabs-custom -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>


@endsection

@push('scripts')
    <script>



        if({{$user->id != Auth::id()}}) {
            document.getElementById("profilebtn").style.display = "none";

        }
        else{

        }


    </script>
    @endpush
