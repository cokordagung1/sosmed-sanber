@extends('layouts.master')

@section('content')
<div class="container">
  <div class="row ml-4">
      <div class="col-sm-12 ml-4">
        <div class="col-md-9">
          <div class="card">
            <div class="card-header p-2">
            </div><!-- /.card-header -->
            <form role="form" method="post" action="/profiles/edit/{{$user->id}}" enctype="multipart/form-data" >
              @csrf
              @method('PUT')
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" value="{{$user->name}}" name="name" class="form-control" id="exampleInputEmail1" placeholder="Enter email" required>
                </div>
                <div class="form-group">
                  <label for="bio">Bio</label><br>
                  <input type="text" name="bio" id="bio"  value="{{$user->userdetail->bio}}" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <div class="input-group">
                    <div class="custom-file">
                        <label for="myfile">Select a file:</label>
                        <input type="file" id="myfile" name="file">
                    </div>
                  </div>
                </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
              </div>
            </form>
          </div>
        </div>
      </div>
  </div>
</div>

@endsection

