<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index');
Route::get('/test',function (){
    return view('home');
});

Auth::routes();


Route::prefix('profiles')->group(function (){
    Route::get('/{id}','ProfileController@index');
    Route::get('/edit/{id}','ProfileController@edit');
    Route::put('/edit/{id}','ProfileController@put');
    Route::get('/detail/create','ProfileController@detail');
    Route::post('/detail/create/{id}','ProfileController@createdetail');
    Route::get('/follow/{id}/{followid}','ProfileController@follow');
    Route::get('/unfollow/{id}/{followid}','ProfileController@unfollow');
});

Route::prefix('post')->group(function (){
   Route::get('/create','PostController@create');
   Route::post('/create/{id}','PostController@post');
    Route::get('/edit/{id}','PostController@edit');
    Route::put('/edit/{id}','PostController@put');
    Route::get('/{id}','PostController@detail');
    Route::get('/delete/{id}','PostController@delete');
    Route::get('/like/{idpost}/{id}','PostController@like');
    Route::get('/liked/{idpost}/{id}','PostController@liked');


});

Route::prefix('comments')->group(function (){
    Route::get('/index', 'CommentController@index');
    Route::post('/comment/{id}/{postid}', 'CommentController@store');
    Route::get('/like/{idpost}/{id}','CommentController@like');
    Route::get('/liked/{idpost}/{id}','CommentController@liked');
});

Route::get('/home', 'HomeController@index')->name('home');
