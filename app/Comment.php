<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = "comment";
    public $timestamps = false;

    public function users(){
        return $this->belongsTo('App\User','users_id','id');
    }

    public function post(){
        return $this->belongsTo('App\Post','post_id','id');
    }
    public function commentlike(){
        return $this->belongsToMany('App\User','comment_like','comment_id','users_id');
    }
}
