<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\User;
use App\CommentLike;

class CommentController extends Controller
{
    public function index(){
         return view('comments.index'); // ini untuk menampilkan INDEX dari comment yg ada,
    }

    public function store($postid,$id,Request $request){
        $comment = new Comment;
        $comment->post_id = $postid;
        $comment->users_id = $id;
        $comment->comment = $request->comment;
        $comment->save();

        return redirect('/');

    }

    public function like($idpost,$id){
        $likepost = new CommentLike();
        $likepost->comment_id = $idpost;
        $likepost->users_id = $id;

        $likepost->save();
        return redirect('/');
    }
    public function liked($idpost,$id){
        $likepost = CommentLike::where('comment_id',$idpost)->where('users_id',$id)->first();
        $likepost->delete();
        return redirect('/');
    }

}
