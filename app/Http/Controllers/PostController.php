<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use App\PostLike;

class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(){
        return view('postss.create');
    }
    public function post($id,Request $request){
        $post = new Post;
        $post->caption = $request->caption;
        $post->type = "text";
        $post->users_id = $id;

        $img = $request->file('file');

        if ($img != null) {
            $nama_file = time() . "_" . $img->getClientOriginalName();
            $tujuan_upload = 'img_post';
            $img->move($tujuan_upload, $nama_file);
            $post->image = $nama_file;
        }


            $post->save();





        return redirect('/');
    }

    public function edit($id){
        $post = Post::find($id);
        return view('postss.edit',['post' => $post]);
    }

    public function put($id,Request $request){
        $post = Post::find($id);
        $post->caption = $request->caption;
        $post->save();

        return redirect('/');
    }

    public function detail($id){
        $post = Post::find($id);
        return view('postss.show');
    }

    public function delete($id){
        $post = Post::find($id);
        $post->delete();

        return redirect('/');
    }

    public function like($idpost,$id){
        $likepost = new PostLike;
        $likepost->post_id = $idpost;
        $likepost->users_id = $id;

        $likepost->save();
        return redirect('/');
    }
    public function liked($idpost,$id){
        $likepost = PostLike::where('post_id',$idpost)->where('users_id',$id)->first();
        $likepost->delete();
        return redirect('/');
    }
}
