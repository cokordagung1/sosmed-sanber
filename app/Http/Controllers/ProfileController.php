<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\UserDetail;
use App\Post;
use App\ProfileFollower;


class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id){
        $user = User::find($id);
        $post = Post::all()->where('users_id',$id);
        $follow = ProfileFollower::all()->first();


        return view('profiles.index',['user' => $user],['post'=> $post ],['follow' => $follow]);
    }

    public function edit($id){
        $user = User::all()->where('id',$id)->first();
        return view('profiles.edit',['user' => $user]);
    }

    public function put($id, Request $request){

       $img = $request->file('file');

       if ($img == null){
           $user = User::find($id);
           $user->name = $request->name;
           $user->save();

           $detail = UserDetail::find($id);
           $detail->bio = $request->bio;
           $detail->save();
       } else{
           $nama_file = time()."_".$img->getClientOriginalName();
           $tujuan_upload = 'profile_img';
           $img->move($tujuan_upload,$nama_file);

           $user = User::find($id);
           $user->name = $request->name;
           $user->save();

           $detail = UserDetail::find($id);
           $detail->bio = $request->bio;
           $detail->img = $nama_file;
           $detail->save();
       }




        return redirect('/');


    }

    public function detail(){
        return view('profiles.create');
    }

    public function createdetail($id,Request $request){
        $detail = new UserDetail;
        $detail->profile_id = $id;
        $detail->bio = $request -> bio;


        $img = $request->file('file');

        if ($img != null) {
            $nama_file = time() . "_" . $img->getClientOriginalName();
            $tujuan_upload = 'profile_img';
            $img->move($tujuan_upload, $nama_file);
            $detail->img = $nama_file;
        }

        $detail->save();

        return redirect('/');
    }

    public function follow($id,$followid){
        $follow = new ProfileFollower();
        $follow->users_id = $id;
        $follow->users_following_id = $followid;

        $follow->save();

        return redirect('/');

    }

    public function unfollow($id,$followid){
        $follow = ProfileFollower::all()->where('users_id',$id)->where('users_following_id',$followid)->first();

        $follow->delete();
        return redirect('/');

    }




}
