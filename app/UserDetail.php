<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $table = "users_detal";
    protected $primaryKey = 'profile_id';
    public $timestamps = false;
    public  function users(){

        return $this->belongsTo('App\User','profile_id','id');
    }

}
