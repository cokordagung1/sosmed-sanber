<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileFollower extends Model
{
    protected $table = "profile_follower";
    public $timestamps = false;
//    public function followerid(){
//        return $this->belongsToMany('App\User','users','users_id','id');
//    }
//
//    public function followingid(){
//        return $this->belongsToMany('App\user','users','users_following_id','id');
//    }
}
