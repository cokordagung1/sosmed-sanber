<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $primaryKey = 'id';

    public function userdetail(){
        return $this->hasOne('App\UserDetail','profile_id','id');
    }

    public function post(){
        return $this->hasMany('App\Post','users_id','id');
    }

    public function comment(){
        return $this->hasMany('App\Comment','users_id','id');
    }

    public function postlike(){
        return $this->belongsToMany('App\PostLike','users_id');
    }

    public function commentlike(){
        return $this->belongsToMany('App\CommentLike','users_id');
    }

    public function followerid(){
        return $this->belongsToMany('App\User','profile_follower','users_following_id','users_id');
    }
    public function followingid(){
        return $this->belongsToMany('App\User','profile_follower','user_id','users_following_id');
    }
    public function commentlikeid(){
        return $this->belongsToMany('App\Comment','comment_like','comment_id','users_id');
    }

    public function postlikeid(){
        return $this->belongsToMany('App\Post','post_like','users_id','post_id');

    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
