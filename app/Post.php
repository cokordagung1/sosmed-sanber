<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Post extends Model
{
    protected $table = "post";
    public $timestamps = false;

    public function users(){
        return $this->belongsTo('App\User','users_id','id');
    }
    public function comment(){
        return $this->hasMany('App\Comment','post_id','id');
    }
    public function postlikeid(){
        return $this->belongsToMany('App\User','post_like','post_id','users_id');

    }
}
