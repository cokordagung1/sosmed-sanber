<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProfileFollower extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_follower', function (Blueprint $table){
            $table->bigIncrements('id');
           $table->unsignedBigInteger('users_id');
           $table->unsignedBigInteger('users_following_id');
           $table->foreign('users_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('users_following_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_follower');
    }
}
